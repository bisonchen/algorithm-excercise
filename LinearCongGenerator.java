import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by bin.chen on 3/24/15.
 */
public class LinearCongGenerator {
    public static void main(String[] args) {
        //A B X1 K M
        Scanner cin=new Scanner(System.in);
        int a = cin.nextInt();
        int b = cin.nextInt();
        int x = cin.nextInt();
        int k = cin.nextInt();
        int m = cin.nextInt();
        LinearCongGenerator solution = new LinearCongGenerator();
        List<Integer> res = solution.generate(a, b, x, k, m);
        for(int i : res) {
            System.out.println(i);
        }
    }

    public List<Integer> generate(int a, int b, int x, int k, int m) {
        List<Integer> res = new ArrayList<Integer>();
        List<Integer> intermediate = new ArrayList<Integer>();
        intermediate.add(x);
        int count =1;
        int prev = 0, loopSize = 0;

        while (true) {
            x = (a*x +b) % m;
            if(count == k +4 ) {
                break;
            }
            if(intermediate.contains(x)) {
                prev = intermediate.indexOf(x); //1,2,3,4,5,1   prev = 1
                loopSize = intermediate.size() - prev;
                break;
            }
            count ++;
            intermediate.add(x);
        }

        if(count == k+4) {
            return intermediate.subList(intermediate.size()-5, intermediate.size());
        } else {
            k = (k - prev - 1) % loopSize;
            List<Integer> loop = intermediate.subList(prev, intermediate.size());
            for(int j=0; j<5; k++, j++) {
                if(k == loop.size()) {
                    k =0;
                }
                res.add(loop.get(k));
            }
            return res;
        }
    }
}
