import java.util.Scanner;

/**
 * Created by bin.chen on 3/24/15.
 */
public class CableCheck {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        int numOfComputers =cin.nextInt();
        int numOfInstructions = cin.nextInt();
        cin.nextLine();

        CableCheck solution = new CableCheck();
        int[][] connections = new int[numOfComputers][numOfComputers];
        solution.init(connections);
        for (int i = 0; i < numOfInstructions; i++) {
            String instruction = cin.next();
            int from = cin.nextInt();
            int to = cin.nextInt();
            int time = cin.nextInt();
            cin.nextLine();
            if(instruction.equals("make")) {
                solution.make(connections, from - 1, to - 1, time);
            } else if(instruction.equals("check")) {
                boolean res = solution.check(connections, from - 1, to -1, time);
                if(res) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            } else {
                throw new RuntimeException();
            }
        }
    }

    private void init(int[][] connection) {
        for(int i=0; i<connection.length; i++) {
            connection[i][i] = Integer.MAX_VALUE;
        }
    }

    private void make(int[][] connection, int node1, int node2, int time) {
        if(connection[node1][node2] >= time) {
            return;
        }
        connection[node1][node2] = time;
        connection[node2][node1] = time;
        for(int i=0; i< connection.length; i++) {
            if(i == node1 || i==node2) {
                continue;
            }
            if(connection[node1][i] < Math.min(connection[node2][i], time)) {
                make(connection, node1, i, Math.min(connection[node2][i], time));
            }
            if(connection[node2][i] < Math.min(connection[node1][i], time)) {
                make(connection, node2, i, Math.min(connection[node1][i], time));
            }
        }
    }

    private boolean check(int[][] connection, int node1, int node2, int time) {
        return connection[node1][node2] >= time;
    }
}
