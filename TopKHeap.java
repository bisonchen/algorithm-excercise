import java.util.*;

public class TopKHeap {

    public List<Integer> topK(List<Integer> list, int k) {
        PriorityQueue<Integer> heap = new PriorityQueue<Integer>(k);
        for(int i: list) {
            if(heap.size() < k || heap.peek() < i) {
                if(heap.size() == k) {
                    heap.remove(heap.peek());
                }
                heap.add(i);
            }
        }
        List<Integer> res = new ArrayList<Integer>(heap);
        Collections.reverse(res);
        return res;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 3, 5, 7, 2, 4 , 6, 8);
        System.out.println(new TopKHeap().topK(list, 3));
    }
}
