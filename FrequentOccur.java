import java.util.*;

/**
 * Created by bin.chen on 3/27/15.
 */

class Elem implements Comparable<Elem>{
    char ch;
    int count;

    public Elem(char ch, int count) {
        this.ch = ch;
        this.count = count;
    }

    @Override
    public int compareTo(Elem o) {
        if(count > o.count) {
            return 1;
        } else if(count < o.count) {
            return -1;
        } else {
            return o.ch - ch;
        }
    }
}

public class FrequentOccur {
    public static void main(String[] args) {
        Scanner ci = new Scanner(System.in);
        String str = ci.nextLine();
        int[] count = new int[26];
        for(char c: str.toCharArray()) {
            count[c-'a'] ++;
        }
        List<Elem> list = new ArrayList<Elem>();
        for(int i=0; i<26; i++) {
            if(count[i] !=0) {
                list.add(new Elem((char) (i+'a'), count[i]));
            }
        }
        Collections.sort(list);
        Collections.reverse(list);
        for(Elem e : list) {
            System.out.println(e.ch);
        }
    }
}
