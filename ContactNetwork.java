import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ContactNetwork {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        int numOfStudents =cin.nextInt();
        cin.nextLine();
        boolean[][] isFriend = new boolean[numOfStudents][numOfStudents];
        for(int i=0; i< numOfStudents; i++) {
            String line = cin.nextLine();
            for(int j=0; j<line.length(); j++) {
                isFriend[i][j] = line.charAt(j) == 'Y' ?  true: false;
            }
        }

        ContactNetwork solution = new ContactNetwork();
        System.out.println(solution.expectedNum(isFriend));
    }

    public double expectedNum(boolean[][] isFriend) {
        double[] result = new double[1];
        boolean[] recevied = new boolean[isFriend.length];
        recevied[0] = true;
        deliverInfo(isFriend, recevied, result, 1, 0, 1);
        return result[0];
    }


    private List<Integer> notDelivered(boolean[][] isFriend, boolean[] received, int cur) {
        List<Integer> res = new ArrayList<Integer>();
        for(int i=0; i<isFriend.length; i++) {
            if(isFriend[cur][i] && !received[i]){
                res.add(i);
            }
        }
        return res;
    }

    private void deliverInfo(boolean[][] isFriend, boolean[] received, double[] result, double chance, int cur, int count) {
        List<Integer> notDelivered = notDelivered(isFriend, received, cur);
        if(notDelivered.size() == 0) {
//            System.out.println("count = "  + count + "; " + "chance = " + chance);
            result[0] += count * chance;
            return;
        }
        for(int next: notDelivered) {
            received[next] = true;
            deliverInfo(isFriend, received, result, chance / notDelivered.size(), next, count+1);
            received[next] = false;
        }
    }
}