import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: chenbin
 * Date: 4/5/15
 * Time: 5:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class Dijkstra {
    public int shortestDist(int[][] graph, int src, int dest) {
        int[] dist = new int[graph.length];
        int[] prev = new int[graph.length];

        for(int i=0; i< graph.length; i++) {
            dist[i] = Integer.MAX_VALUE;
            prev[i] = -1;
        }
        dist[src] = 0;

        Set<Integer> reachable = new HashSet<Integer>();
        reachable.add(src);

        while (!reachable.isEmpty()) {
            int cur = minDistance(dist, reachable);
            if(cur == dest) {
                return dist[dest];
            }
            for(int i: adjacent(graph, cur)) {
                if(dist[cur] + graph[cur][i] < dist[i]) {
                    dist[i] = dist[cur] + graph[cur][i];
                    prev[i] = cur;
                    if(!reachable.contains(i)) {
                        reachable.add(i);
                    }
                }
            }
        }
        return Integer.MAX_VALUE;
    }

    private int minDistance(int[] dist, Set<Integer> reachable) {
        assert reachable.size() > 0;
        int min= Integer.MAX_VALUE, ret = -1;
        for(int i : reachable) {
            if(dist[i] < min) {
                min = dist[i];
                ret = i;
            }
        }
        reachable.remove(ret);
        return ret;
    }

    private Set<Integer> adjacent(int[][] graph, int src) {
        Set<Integer> adjacent = new HashSet<Integer>();
        for(int i=0; i< graph.length; i++) {
            if(i != src && graph[src][i] != 0) {
                adjacent.add(i);
            }
        }
        return adjacent;
    }

    public static void main(String[] args) {
        int graph[][] = new int[][]{{0, 4, 0, 0, 0, 0, 0, 8, 0},
            {4, 0, 8, 0, 0, 0, 0, 11, 0},
            {0, 8, 0, 7, 0, 4, 0, 0, 2},
            {0, 0, 7, 0, 9, 14, 0, 0, 0},
            {0, 0, 0, 9, 0, 10, 0, 0, 0},
            {0, 0, 4, 0, 10, 0, 2, 0, 0},
            {0, 0, 0, 14, 0, 2, 0, 1, 6},
            {8, 11, 0, 0, 0, 0, 1, 0, 7},
            {0, 0, 2, 0, 0, 0, 6, 7, 0}};

        Dijkstra dijkstra = new Dijkstra();
        for(int i=0; i<9; i++) {
            System.out.println(dijkstra.shortestDist(graph, 0, i));

        }
    }
}
